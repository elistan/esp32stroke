## Getting started

### Get the pieces. 

You will need 
- an ESP32 controller (e.g. https://www.amazon.com/dp/B0C7C2HQ7P) - the brain of the system, and the interface to your computer or mobile.
- Breadboard (e.g https://www.amazon.com/dp/B099MQV8ZW - this is kind of "lego for electronics" - just press in things the right way. The kit also has all needed wires, some LEDs and resistors that are useful for visulizing and checking things) 
- Alternatively - if you can - a bit of soldering skills and some resistors,LEDs and wire, that you then likely have. (If you can't solder, be sure to order the ESP with Pin headers)
- The device and software currently support 8 estim channels and 8 auxiliary channels that just turn on and off and have timers.
- So per estim channel you want to use, you will need:
    - an audio transformer for providing galvanic insulation (e.g. https://www.amazon.com/dp/B0CB67NQG1) - note that the one linked is a bit on the weak side. The exact model is not very important. Usual AC transformers work well, too. ~1:100 ratio recommended.
    - if you plan to run many channels, darlington-ICs like ULN-2003A (https://www.amazon.com/dp/B0CBM23ZJ3) might come in handy. They have 7 channels per chip. 
    - alternatively, per channel you will need a power transistor like the TIP 120 (e.g. https://www.amazon.com/dp/B0CBKDN3R5 - similar Darlington types will do), in that case a 220 Ohm resistor as current limiter for the transistor is needed, too. (not critical 470 will do, too)
    - optionally an LED (recommended as you see what happens. Use red due to the low voltage drop)


### Put things together

1. Look at the [schematics](schematics.pdf) or [the 7 channel ULN20023 version](schematics7.pdf). Even if you can't read schematics, it may still help you when you just follow the breadboard instructions.
Schematic reading for beginners: If two lines intersect, they are not connected. Only when a little dot is at the intersection, that's a connection.
2. a) Look at 
![Breadboard](breadboard.png) 
for how to wire the Breadboard, for 1 channel, assuming you put the ESP32 flush left into the breadboard.
This will look like 
![this](fromSide.png) 
and 
![this](fromTop.png)
in real life.
2. b) Or look at 
![Breadboard7](breadboard7.png)
for how to wire the Breadboard, for 7 channels with an ULN-2003A.

3. Use esptool (see https://www.flamingo-tech.nl/2021/03/21/installing-and-using-esptools/ for instructions) to flash the software onto the ESP32.
The relevant command line is
    esptool --chip esp32 -b 460800 --before default_reset --after hard_reset write_flash --flash_mode dio --flash_size 2MB --flash_freq 40m \
            0x1000 build/bootloader/bootloader.bin \
            0x8000 build/partition_table/partition-table.bin \
            0x10000 build/esp32stroke.bin
4. Start building.
    1. Place the leftmost 2 wires to get 5V and GND from the ESP chip to the rails on the top
    If you wish to, you can add a small capacitor - say 100µF - between the two lines to provide a bit of backing and decoupling during pulses.
    Actually, most ESP32 boards provide a very weak 5V line, so I actually recommend not to get the 5V from the ESP as shown on the 1-transistor schematics, but rather use another power source, as shown in the ULN-2003A schematics. A 9V block battery will do nicely. Please mind the polarity.
    Do *not* wire both the ESP 32 and an external power source to the + rail. The - rail needs to be connected, though.
    2. Place the long wire to connect the first channel (G12, will be channel 0 in the app) to the resistor and from there to the LED.
        - If you want to do a quick test at this point, put the cathode pin of the LED in the blue GND rail
        - If programming was successful, it should slowly pulse, very dimly.
        - Place cathode pin back in its own row as shown
    3. Place the transistor so that its left pin (Base - check if you used another transistor type) is on the same row as the LED cathode.
    5. Place the short wire from the blue GND rail to the right transistor pin (Emitter). 
    Just like in the above test, the LED should slowly pulse
    6. Place the audio transformer. The type I linked in the part list above has an etched piece of writing - that goes to the lower end. Otherwise you can try it out. One direction should provide considerably more oomph.
    Ensure one pin of the audio transformer connects to the middle (Collector) pin of the transistor.
    The other pin goes to the red 5V rail.
    7. Attach two outgoing wires to the lower connectors of the transformer.
5. Done. Easy, ain't it?
6. Optional:
    1. Repeat as much as you like with the pins G14, G27, G26, G25, G33, G32, G13, which are channels 1-7.
    2. If you also want to go for the toggle/timer channels (great to control magnetic locks ...), do the same with G23, G22, G21, G19, G18, G5, G4, G2, which you will find on the lower pin row. Do not connect audio transformers there. You can instead separate the power supply of any low voltage toy and turn it on and off with it. In priciple this also works with the estim channels, which may allow you to control the speed.
    You need to connect the ground from the toy to the overall ground (GND rail) and the return feed of the toy to ground to the transistor output.
    For battery operated devices, you can slip a piece of double sided PCB under the most negative end of the battery pack. Connect the battery side to GND, the connector-in-the-housing side to the collector pin and it should work. The TIP is pretty robust - if you do it wrong, the bypass diodes kick in. If the toy goes to always on, it's wrong. 

### Testing it

1. get strokegui.html from the repository or navigate to https://elistan.gitlab.io/esp32stroke/strokegui.html , where it is hosted right here on gitlab.
2. Doubleclick it. It will show the following Screen in your webbrowser:

![GUI](strokegui.png){.float-right}

3. If you have the device connected by USB wire, press "Serial". Note that some browsers do not implement webserial or you need to enable it - see https://developer.mozilla.org/en-US/docs/Web/API/Web_Serial_API#browser_compatibility for a table. 
Alternatively you can try a Bluetoth conection via pressing BLE. The device will show up as LVS-A011 (intended for lovense device emulation).
Note: On mobile, the serial port is usually not implemented on the browser. So even if you power the device via your mobile and OTG cable, still use BL to control. 
4. To select which pin's signal to control click on the 8 Channel tabs on the top. A green frame shows the active one. If you doubleclick them, you can name the channels. This also selects the video to show in the tab (see below).
5. Tune the Power Limit all the way up. This will later enable you to limit how much a cannel can deliver. This setting cannot be overwritten remotely (safety feature).
6. Play with the Power slider - the LED should react to it. It controls the maximum power during the pulses
7. Now try Min-Power. This controls how far the power is reduces during the low phase of the pulses.
8. Try speed. The LED should blink faster and slower
9. Try Duty cycle. The LED should be more at min or more at max setting
10. Frequency: Adjust to your preferences. Muscles react stronger to 20-150 Hz, while 300-700 is more in for other sensations. YMMV also with electrode placement. Note that Frequency is global and applies to *all* Channels. Be careful when changing that.
11. Videos: The tabs will show a short looping video, based on the channel name. For demo puposes, name one of the channels "coffee". A corresponding video is available here on gitlab. I'll have a look on where I could host more appropriate content.

### Going live

1. be sure you understand the basic safety rules for electrostimulation. not above waist, no current paths close to vital organs.
2. for a first test, hold the two wire ends from the transformer between wetted fingers of *the same* hand.
3. If nothing happens, use a LED+Resistor combination to see if the transformer connects well. They tend not to stick well into the breadboard

## Troubleshooting

1. LED works fine, but no output after transformer
    Check output with a simple Output-pin1 -> R220 -> LED -> Output-pin2 connection. Lights up? All good, maybe try to get a better connection to yourself.
    Could also be the transformer is in the wrong way, transforming down instead of up. turn 180° to try.
    1. If not: same circuit at input side of the transformer. doesn't light up when LED is oriented correctly? check transistor
    2. If that is ok, check if you have the right transformer pins. Input side shuld be connected to two pins that have ~ 8 ohms betwen them.
    If you have no multimeter to check, just try turning 90°. There is just 2 cases.

/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "driver/gpio.h"
#include "driver/ledc.h"
#include "esp_system.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_bt.h"
#include "esp_flash.h"
#include <esp_chip_info.h>

#include <esp_gap_ble_api.h>
#include "esp_gatts_api.h"
#include "esp_bt_defs.h"
#include "esp_bt_main.h"
#include "esp_gatt_common_api.h"
#include <esp_timer.h>

#define BLINK_GPIO GPIO_NUM_32

#define GATTS_TAG "Estim"
#include "sdkconfig.h"


///Declare the static function
static void gatts_profile_a_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param);

#define GATTS_SERVICE_UUID_TEST_A   0xFFF0
#define GATTS_CHAR_UUID_TEST_A1     0x0002
#define GATTS_CHAR_UUID_TEST_A2     0x0003
#define GATTS_DESCR_UUID_TEST_A     0x3333
#define GATTS_NUM_HANDLE_TEST_A     8

#define TEST_DEVICE_NAME            "LVS-A011"
#define TEST_MANUFACTURER_DATA_LEN  17

#define GATTS_DEMO_CHAR_VAL_LEN_MAX 0x40

#define PREPARE_BUF_MAX_SIZE 1024

static uint8_t char1_str[] = {0x11,0x22,0x33};
static uint8_t char2_str[] = {0x12,0x34,0x56};

static esp_gatt_char_prop_t a1_property = 0;

static esp_attr_value_t gatts_demo_char_val[2] = {
	{
		.attr_max_len = GATTS_DEMO_CHAR_VAL_LEN_MAX,
		.attr_len     = sizeof(char1_str),
		.attr_value   = char1_str,
	},
	{
		.attr_max_len = GATTS_DEMO_CHAR_VAL_LEN_MAX,
		.attr_len     = sizeof(char2_str),
		.attr_value   = char2_str,
	}
};

static uint8_t adv_config_done = 0;
#define adv_config_flag      (1 << 0)
#define scan_rsp_config_flag (1 << 1)

static uint8_t adv_service_uuid128[16] = {
    /* LSB <--------------------------------------------------------------------------------> MSB */
    // first uuid, 16bit, [12],[13] is the value
    0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0xf0, 0xff, 0x00, 0x00,
	// 0x9e, 0xca, 0xdc, 0x24, 0x0e, 0xe5, 0xa9, 0xe0, 0xf3, 0x93, 0xa3, 0xb5, 0x01, 0x00, 0x40, 0x6e, //6e400001-b5a3-f393-e0a9-e50e24dcca9e
	//0x53, 0x56, 0x4c, 0x0e, 0x92, 0xa6, 0xd5, 0xbb, 0xd4, 0x4b, 0x23, 0x00, 0x02, 0x00, 0x30, 0x42 // 42300002-0023-4bd4-bbd5-a6920e4c5653
};

// The length of adv data must be less than 31 bytes
//static uint8_t test_manufacturer[TEST_MANUFACTURER_DATA_LEN] =  {0x12, 0x23, 0x45, 0x56};
//adv data
static esp_ble_adv_data_t adv_data = {
    .set_scan_rsp = false,
    .include_name = true,
    .include_txpower = false,
    .min_interval = 0x0006, //slave connection min interval, Time = min_interval * 1.25 msec
    .max_interval = 0x0010, //slave connection max interval, Time = max_interval * 1.25 msec
    .appearance = 0x00,
    .manufacturer_len = 0, //TEST_MANUFACTURER_DATA_LEN,
    .p_manufacturer_data =  NULL, //&test_manufacturer[0],
    .service_data_len = 0,
    .p_service_data = NULL,
    .service_uuid_len = sizeof(adv_service_uuid128),
    .p_service_uuid = adv_service_uuid128,
    .flag = (ESP_BLE_ADV_FLAG_GEN_DISC | ESP_BLE_ADV_FLAG_BREDR_NOT_SPT),
};
// scan response data
static esp_ble_adv_data_t scan_rsp_data = {
    .set_scan_rsp = true,
    .include_name = true,
    .include_txpower = true,
    //.min_interval = 0x0006,
    //.max_interval = 0x0010,
    .appearance = 0x00,
    .manufacturer_len = 0, //TEST_MANUFACTURER_DATA_LEN,
    .p_manufacturer_data =  NULL, //&test_manufacturer[0],
    .service_data_len = 0,
    .p_service_data = NULL,
    .service_uuid_len = sizeof(adv_service_uuid128),
    .p_service_uuid = adv_service_uuid128,
    .flag = (ESP_BLE_ADV_FLAG_GEN_DISC | ESP_BLE_ADV_FLAG_BREDR_NOT_SPT),
};

static esp_ble_adv_params_t adv_params = {
    .adv_int_min        = 0x20,
    .adv_int_max        = 0x40,
    .adv_type           = ADV_TYPE_IND,
    .own_addr_type      = BLE_ADDR_TYPE_PUBLIC,
    //.peer_addr            =
    //.peer_addr_type       =
    .channel_map        = ADV_CHNL_ALL,
    .adv_filter_policy = ADV_FILTER_ALLOW_SCAN_ANY_CON_ANY,
};

#define PROFILE_NUM 3
#define PROFILE_A_APP_ID 0
#define PROFILE_A_APP_ID1 1

struct gatts_chardesc_list {
	uint16_t handle;
	esp_bt_uuid_t uuid;
	esp_gatt_perm_t perm;
	esp_gatt_char_prop_t property;
};

struct gatts_profile_inst {
    esp_gatts_cb_t gatts_cb;
    uint16_t gatts_if;
    uint16_t app_id;
    uint16_t conn_id;
    uint16_t service_handle;
    esp_gatt_srvc_id_t service_id;
	uint16_t num_chars;
	struct gatts_chardesc_list *chardesclist;
};

#define GATTS_CHAR_NUMS 5
static struct gatts_chardesc_list gl_chardesc_tab[GATTS_CHAR_NUMS] = {
	{ .handle = 0, .uuid.len = ESP_UUID_LEN_128, .uuid.uuid.uuid128 = { 0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0xf1, 0xff, 0x00, 0x00 }, 
	  .perm = ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE, 
	  .property = ESP_GATT_CHAR_PROP_BIT_READ | ESP_GATT_CHAR_PROP_BIT_WRITE | ESP_GATT_CHAR_PROP_BIT_NOTIFY,
	},
	{ .handle = 0, .uuid.len = ESP_UUID_LEN_16, .uuid.uuid.uuid16 = ESP_GATT_UUID_CHAR_CLIENT_CONFIG, 
	  .perm = ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE, 
	  .property = ESP_GATT_CHAR_PROP_BIT_READ | ESP_GATT_CHAR_PROP_BIT_WRITE | ESP_GATT_CHAR_PROP_BIT_NOTIFY,
	},
	{ .handle = 0, .uuid.len = ESP_UUID_LEN_128, .uuid.uuid.uuid128 = { 0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0xf2, 0xff, 0x00, 0x00 }, 
	  .perm = ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE, 
	  .property = ESP_GATT_CHAR_PROP_BIT_READ | ESP_GATT_CHAR_PROP_BIT_WRITE | ESP_GATT_CHAR_PROP_BIT_NOTIFY,
	},
	{ .handle = 0, .uuid.len = ESP_UUID_LEN_16, .uuid.uuid.uuid16 = ESP_GATT_UUID_CHAR_CLIENT_CONFIG, 
	  .perm = ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE, 
	  .property = ESP_GATT_CHAR_PROP_BIT_READ | ESP_GATT_CHAR_PROP_BIT_WRITE | ESP_GATT_CHAR_PROP_BIT_NOTIFY,
	},
	{ .handle = 0, .uuid.len = ESP_UUID_LEN_16, .uuid.uuid.uuid16 = 0x2901, 
	  .perm = ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE, 
	  .property = ESP_GATT_CHAR_PROP_BIT_READ | ESP_GATT_CHAR_PROP_BIT_WRITE | ESP_GATT_CHAR_PROP_BIT_NOTIFY,
	}
};

/* One gatt-based profile one app_id and one gatts_if, this array will store the gatts_if returned by ESP_GATTS_REG_EVT */
static struct gatts_profile_inst gl_profile_tab[PROFILE_NUM] = {
    [PROFILE_A_APP_ID] = {
        .gatts_cb = gatts_profile_a_event_handler,
        .gatts_if = ESP_GATT_IF_NONE,       /* Not get the gatt_if, so initial is ESP_GATT_IF_NONE */
		.chardesclist = gl_chardesc_tab,
		.num_chars = GATTS_CHAR_NUMS,
		.service_id.id.uuid.len = ESP_UUID_LEN_128,
		.service_id.id.uuid.uuid.uuid128 = { 0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0xf0, 0xff, 0x00, 0x00 },	
    }
};

#define NUM_CHANNELS 8
#define NUM_TCHANNELS 8

enum mode_preset {
	MP_TRIANGLE, MP_SQUARE, MP_SAWTOOTH, MP_DECAY
};

struct channel_desc {
	ledc_channel_t channel;
	int gpio;
	int32_t laststart, periode, count;
	uint16_t attack, hold, decay, low, high, limit;
	enum mode_preset mode;
	uint8_t duty_cycle;
};

static struct channel_desc channels[NUM_CHANNELS] = {
	{ LEDC_CHANNEL_0, GPIO_NUM_12, 0, 20000, -1, 0x4000, 0x8000, 0xc000, 0, 5, 999, MP_TRIANGLE, 50 },
	{ LEDC_CHANNEL_1, GPIO_NUM_14, 0, 20000, -1, 0x4000, 0x8000, 0xc000, 0, 5, 999, MP_TRIANGLE, 50 },
	{ LEDC_CHANNEL_2, GPIO_NUM_27, 0, 20000, -1, 0x4000, 0x8000, 0xc000, 0, 5, 999, MP_TRIANGLE, 50 },
	{ LEDC_CHANNEL_3, GPIO_NUM_26, 0, 20000, -1, 0x4000, 0x8000, 0xc000, 0, 5, 999, MP_TRIANGLE, 50 },
	{ LEDC_CHANNEL_4, GPIO_NUM_25, 0, 20000, -1, 0x4000, 0x8000, 0xc000, 0, 5, 999, MP_TRIANGLE, 50 },
	{ LEDC_CHANNEL_5, GPIO_NUM_33, 0, 20000, -1, 0x4000, 0x8000, 0xc000, 0, 5, 999, MP_TRIANGLE, 50 },
	{ LEDC_CHANNEL_6, GPIO_NUM_32, 0, 20000, -1, 0x4000, 0x8000, 0xc000, 0, 5, 999, MP_TRIANGLE, 50 },
	{ LEDC_CHANNEL_7, GPIO_NUM_13, 0, 20000, -1, 0x4000, 0x8000, 0xc000, 0, 5, 999, MP_TRIANGLE, 50 }
};

struct toggle_desc {
    int gpio, onoff;
    int32_t timer;
};

static struct toggle_desc tchannels[NUM_TCHANNELS] = {
    { GPIO_NUM_23, 0, 0 },
    { GPIO_NUM_22, 0, 0 },
    { GPIO_NUM_21, 0, 0 },
    { GPIO_NUM_19, 0, 0 },
    { GPIO_NUM_18, 0, 0 },
    { GPIO_NUM_5, 0, 0 },
    { GPIO_NUM_4, 0, 0 },
    { GPIO_NUM_2, 0, 0 }
};

ledc_channel_config_t ledc_channel_template = {
	.channel    = LEDC_CHANNEL_0,
	.duty       = 0,
	.gpio_num   = BLINK_GPIO,
	.speed_mode = LEDC_HIGH_SPEED_MODE,
	.hpoint     = 0,
	.timer_sel  = LEDC_TIMER_0
};

static void setup_channel(struct channel_desc *channel) {
	printf("Setup channel mode %d, dc %d\n", channel->mode, channel->duty_cycle);
	fflush(stdout);
	switch(channel->mode) {
		case MP_TRIANGLE:
			if (channel->duty_cycle <= 50) {
				channel->decay = 0xffff * channel->duty_cycle / 50;
				channel->attack = channel->hold = channel->decay / 2;
			} else {
				channel->hold = 0xffff * (channel->duty_cycle-50) / 50;
				channel->attack = channel->decay = (0xffff - channel->hold) / 2;
				channel->hold += channel->attack;
				channel->decay += channel->hold;
			}
			break;
		case MP_SQUARE:
			channel->attack = 0;
			channel->hold = channel->decay = 0xffff * channel->duty_cycle / 100;
			break;
		case MP_SAWTOOTH:
			if (channel->duty_cycle <= 50) {
				channel->attack = channel->hold = channel->decay = 0xffff * channel->duty_cycle / 50;
			} else {
				channel->hold = 0xffff * (channel->duty_cycle-50) / 50;
				channel->attack = 0xffff - channel->hold;
				channel->hold += channel->attack;
				channel->decay = 0xffff;
			}
			break;
		case MP_DECAY:
		        channel->attack = 0;
			if (channel->duty_cycle <= 50) {
				channel->hold = 0; 
				channel->decay = 0xffff * channel->duty_cycle / 50;
			} else {
				channel->hold = 0xffff * (channel->duty_cycle-50) / 50;
				channel->decay = 0xffff;
			}
			break;
	}
	printf("Setup channel %04x %04x %04x\n", channel->attack, channel->hold, channel->decay);
	fflush(stdout);
}

static struct gatts_chardesc_list *getCharacteristicByHandle(uint16_t handle, struct gatts_chardesc_list *list, uint16_t num_entries) {
	for( uint16_t i = 0; i < num_entries ; i++) {
		if (list[i].handle == handle) return &list[i];
	}
	return NULL;
}

typedef struct {
    uint8_t                 *prepare_buf;
    int                     prepare_len;
} prepare_type_env_t;

static prepare_type_env_t a_prepare_write_env;

void example_write_event_env(esp_gatt_if_t gatts_if, prepare_type_env_t *prepare_write_env, esp_ble_gatts_cb_param_t *param);
void example_exec_write_event_env(prepare_type_env_t *prepare_write_env, esp_ble_gatts_cb_param_t *param);

static void gap_event_handler(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param)
{
    switch (event) {
    case ESP_GAP_BLE_ADV_DATA_SET_COMPLETE_EVT:
        adv_config_done &= (~adv_config_flag);
        if (adv_config_done == 0){
            esp_ble_gap_start_advertising(&adv_params);
        }
        break;
    case ESP_GAP_BLE_SCAN_RSP_DATA_SET_COMPLETE_EVT:
        adv_config_done &= (~scan_rsp_config_flag);
        if (adv_config_done == 0){
            esp_ble_gap_start_advertising(&adv_params);
        }
        break;
    case ESP_GAP_BLE_ADV_START_COMPLETE_EVT:
        //advertising start complete event to indicate advertising start successfully or failed
        if (param->adv_start_cmpl.status != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(GATTS_TAG, "Advertising start failed\n");
        }
        break;
    case ESP_GAP_BLE_ADV_STOP_COMPLETE_EVT:
        if (param->adv_stop_cmpl.status != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(GATTS_TAG, "Advertising stop failed\n");
        } else {
            ESP_LOGI(GATTS_TAG, "Stop adv successfully\n");
        }
        break;
    case ESP_GAP_BLE_UPDATE_CONN_PARAMS_EVT:
         ESP_LOGI(GATTS_TAG, "update connection params status = %d, min_int = %d, max_int = %d,conn_int = %d,latency = %d, timeout = %d",
                  param->update_conn_params.status,
                  param->update_conn_params.min_int,
                  param->update_conn_params.max_int,
                  param->update_conn_params.conn_int,
                  param->update_conn_params.latency,
                  param->update_conn_params.timeout);
        break;
    default:
        break;
    }
}

void example_write_event_env(esp_gatt_if_t gatts_if, prepare_type_env_t *prepare_write_env, esp_ble_gatts_cb_param_t *param){
    esp_gatt_status_t status = ESP_GATT_OK;
    ESP_LOGI(GATTS_TAG, "wr_ev_env in\n");
    if (param->write.need_rsp){
		ESP_LOGI(GATTS_TAG, "wr_ev_env need_rsp\n");
        if (param->write.is_prep){
			ESP_LOGI(GATTS_TAG, "wr_ev_env is_prep\n");
            if (prepare_write_env->prepare_buf == NULL) {
                prepare_write_env->prepare_buf = (uint8_t *)malloc(PREPARE_BUF_MAX_SIZE*sizeof(uint8_t));
                prepare_write_env->prepare_len = 0;
                if (prepare_write_env->prepare_buf == NULL) {
                    ESP_LOGE(GATTS_TAG, "Gatt_server prep no mem\n");
                    status = ESP_GATT_NO_RESOURCES;
                }
            } else {
                if(param->write.offset > PREPARE_BUF_MAX_SIZE) {
                    status = ESP_GATT_INVALID_OFFSET;
                } else if ((param->write.offset + param->write.len) > PREPARE_BUF_MAX_SIZE) {
                    status = ESP_GATT_INVALID_ATTR_LEN;
                }
            }

// huh?
			ESP_LOGI(GATTS_TAG, "wr_ev_env response\n");
            esp_gatt_rsp_t *gatt_rsp = (esp_gatt_rsp_t *)malloc(sizeof(esp_gatt_rsp_t));
            gatt_rsp->attr_value.len = param->write.len;
            gatt_rsp->attr_value.handle = param->write.handle;
            gatt_rsp->attr_value.offset = param->write.offset;
            gatt_rsp->attr_value.auth_req = ESP_GATT_AUTH_REQ_NONE;
            memcpy(gatt_rsp->attr_value.value, param->write.value, param->write.len);
            esp_err_t response_err = esp_ble_gatts_send_response(gatts_if, param->write.conn_id, param->write.trans_id, status, gatt_rsp);
            if (response_err != ESP_OK){
               ESP_LOGE(GATTS_TAG, "Send response error\n");
            }
            free(gatt_rsp);
            if (status != ESP_GATT_OK){
                return;
            }
            memcpy(prepare_write_env->prepare_buf + param->write.offset,
                   param->write.value,
                   param->write.len);
            prepare_write_env->prepare_len += param->write.len;

        } else {
			ESP_LOGI(GATTS_TAG, "wr_ev_env !is_prep\n");
            esp_ble_gatts_send_response(gatts_if, param->write.conn_id, param->write.trans_id, status, NULL);
        }
    }
}

void example_exec_write_event_env(prepare_type_env_t *prepare_write_env, esp_ble_gatts_cb_param_t *param){
    if (param->exec_write.exec_write_flag == ESP_GATT_PREP_WRITE_EXEC){
        esp_log_buffer_hex(GATTS_TAG, prepare_write_env->prepare_buf, prepare_write_env->prepare_len);
    }else{
        ESP_LOGI(GATTS_TAG,"ESP_GATT_PREP_WRITE_CANCEL");
    }
    if (prepare_write_env->prepare_buf) {
        free(prepare_write_env->prepare_buf);
        prepare_write_env->prepare_buf = NULL;
    }
    prepare_write_env->prepare_len = 0;
}

static int regnum = 0;
void gatts_add_chars(void) {

	if (regnum >= gl_profile_tab[PROFILE_A_APP_ID].num_chars) return;
	
	struct gatts_chardesc_list *chardesc = &gl_profile_tab[PROFILE_A_APP_ID].chardesclist[regnum];

	a1_property = ESP_GATT_CHAR_PROP_BIT_READ | ESP_GATT_CHAR_PROP_BIT_WRITE | ESP_GATT_CHAR_PROP_BIT_NOTIFY;
	if (chardesc->uuid.len != ESP_UUID_LEN_16) {
		esp_err_t add_char_ret = 
			esp_ble_gatts_add_char(gl_profile_tab[PROFILE_A_APP_ID].service_handle, &chardesc->uuid,
								   ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE, a1_property, 
								   &gatts_demo_char_val[0], NULL); // fixme char val
		if (add_char_ret){
			ESP_LOGE(GATTS_TAG, "add char failed, error code =%x",add_char_ret);
		}
	} else {
		esp_err_t add_descr_ret = 
			esp_ble_gatts_add_char_descr(gl_profile_tab[PROFILE_A_APP_ID].service_handle, &chardesc->uuid,
										 ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE, NULL, NULL);
		if (add_descr_ret){
			ESP_LOGE(GATTS_TAG, "add char descr failed, error code =%x", add_descr_ret);
		}
	}
	ESP_LOGI(GATTS_TAG, "gatts_add_chars %d, addchar %x\n", regnum, chardesc->uuid.uuid.uuid16);
}

static int32_t getdec(int8_t len, unsigned char *buf) {
	int32_t result = 0;
	while(len--) {
		if (*buf < '0' || *buf > '9') break;
		result *= 10;
		result += *buf - '0';
		buf++;
	}
	//ESP_LOGI(GATTS_TAG, "getdec %d", result);
	return result;
}

static int set_timer_freq(int freq) {
	ledc_timer_config_t ledc_timer = {
		.duty_resolution = LEDC_TIMER_10_BIT, // could do more, but don't really need that
		.freq_hz = freq,
		.speed_mode = LEDC_HIGH_SPEED_MODE,
		.timer_num = LEDC_TIMER_0,
		.clk_cfg = LEDC_AUTO_CLK,
	};
	//ESP_LOGI(GATTS_TAG, "set_timer %lu", ledc_timer.freq_hz);
	ledc_timer_config(&ledc_timer);
	return 0;
}

static int parse_cmd(uint8_t *cmd, int len) {
	if (len <= 0) return -1;
	int channel = -1;
	if (len >= 2) channel = getdec(1, cmd + 1);
	switch(*cmd) {
		case 'f': /* frequency */
			if (len < 5) return -1;
			set_timer_freq(getdec(4,cmd+1));
			return 0;
		case 'l': /* total power limit */
			if (channel < 0 || channel >= NUM_CHANNELS || len < 5) return -1;
			channels[channel].limit = getdec(3, cmd + 2); 
			return 0;
		case 'p': /* upper power limit */
			if (channel < 0 || channel >= NUM_CHANNELS || len < 5) return -1;
			channels[channel].high = getdec(3, cmd + 2); 
			return 0;
		case 'P': /* lower power limit */
			if (channel < 0 || channel >= NUM_CHANNELS || len < 5) return -1;
			channels[channel].low = getdec(3, cmd + 2);
			return 0;
		case 'd': /* speed */
			if (channel < 0 || channel >= NUM_CHANNELS || len < 5) return -1;
			channels[channel].periode = pow(10.0, (500 - getdec(3, cmd + 2)) * 0.002) * 1000;
			return 0;
		case 'c': /* duty cycle */
			if (channel < 0 || channel >= NUM_CHANNELS || len < 4) return -1;
			channels[channel].duty_cycle = getdec(2, cmd + 2);
			setup_channel(&channels[channel]); 
			return 0;
		case 'C': /* Count */
			if (channel < 0 || channel >= NUM_CHANNELS || len < 5) return -1;
			channels[channel].count = getdec(3, cmd + 2);
			if (channels[channel].count == 0) channels[channel].count = -1;
			return 0;
		case 'm': /* "mode" */
			if (channel < 0 || channel >= NUM_CHANNELS || len < 3) return -1;
			switch(cmd[2]) {
				case 't': /* "triangle" */
					channels[channel].mode = MP_TRIANGLE; 
					break;
				case 'p': /* "pulse" */
					channels[channel].mode = MP_SQUARE; 
					break;
				case 's': /* "sawtooth" */
					channels[channel].mode = MP_SAWTOOTH; 
					break;
				case 'd': /* "decay" */
					channels[channel].mode = MP_DECAY; 
					break;
				default:
					return -1;
			}
			setup_channel(&channels[channel]);
			return 0;
                case 'o': /* On/Off*/
                        if (channel < 0 || channel >= NUM_TCHANNELS || len < 3) return -1;
                        switch(cmd[2]) {
                            case '0': 
                                tchannels[channel].onoff = 0;
                                goto set_onoff;
                            case '1':
                                tchannels[channel].onoff = 1;
                            set_onoff:
                                tchannels[channel].timer = 0;
                                gpio_set_level(tchannels[channel].gpio, tchannels[channel].onoff);
                                break;
                        }
                        break;
                case 't': /* Timer */
                        if (channel < 0 || channel >= NUM_TCHANNELS || len < 7) return -1;
			tchannels[channel].timer = getdec(5, cmd + 2) + esp_timer_get_time() / 1000000l;
			break;
		default:
			return -1;
	} // switch
	return 0;
}


static void gatts_profile_a_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param) {
    switch (event) {
    case ESP_GATTS_REG_EVT:
        ESP_LOGI(GATTS_TAG, "REGISTER_APP_EVT, status %d, app_id %d\n", param->reg.status, param->reg.app_id);
        gl_profile_tab[PROFILE_A_APP_ID].service_id.is_primary = true;
        gl_profile_tab[PROFILE_A_APP_ID].service_id.id.inst_id = 0x00;
        //gl_profile_tab[PROFILE_A_APP_ID].service_id.id.uuid.len = ESP_UUID_LEN_128;
        //gl_profile_tab[PROFILE_A_APP_ID].service_id.id.uuid.uuid.uuid16 = { 0x9e, 0xca, 0xdc, 0x24, 0x0e, 0xe5, 0xa9, 0xe0, 0xf3, 0x93, 0xa3, 0xb5, 0x01, 0x00, 0x40, 0x6e };

        esp_err_t set_dev_name_ret = esp_ble_gap_set_device_name(TEST_DEVICE_NAME);
        if (set_dev_name_ret){
            ESP_LOGE(GATTS_TAG, "set device name failed, error code = %x", set_dev_name_ret);
        }
        //config adv data
        esp_err_t ret = esp_ble_gap_config_adv_data(&adv_data);
        if (ret){
            ESP_LOGE(GATTS_TAG, "config adv data failed, error code = %x", ret);
        }
        adv_config_done |= adv_config_flag;
        //config scan response data
        ret = esp_ble_gap_config_adv_data(&scan_rsp_data);
        if (ret){
            ESP_LOGE(GATTS_TAG, "config scan response data failed, error code = %x", ret);
        }
        adv_config_done |= scan_rsp_config_flag;

        esp_ble_gatts_create_service(gatts_if, &gl_profile_tab[PROFILE_A_APP_ID].service_id, GATTS_NUM_HANDLE_TEST_A);
        break;
    case ESP_GATTS_READ_EVT: {
        ESP_LOGI(GATTS_TAG, "GATT_READ_EVT, conn_id %u, trans_id %lu, handle %u\n", param->read.conn_id, param->read.trans_id, param->read.handle);
        esp_gatt_rsp_t rsp;
        memset(&rsp, 0, sizeof(esp_gatt_rsp_t));
        rsp.attr_value.handle = param->read.handle;
        rsp.attr_value.len = 4;
        rsp.attr_value.value[0] = 'C';
        rsp.attr_value.value[1] = ';';
        rsp.attr_value.value[2] = '1';
        rsp.attr_value.value[3] = 0;
        esp_ble_gatts_send_response(gatts_if, param->read.conn_id, param->read.trans_id,
                                    ESP_GATT_OK, &rsp);
        break;
    }
    case ESP_GATTS_WRITE_EVT: {
        ESP_LOGI(GATTS_TAG, "GATT_WRITE_EVT, conn_id %u, trans_id %lu, handle %u", param->write.conn_id, param->write.trans_id, param->write.handle);
        if (!param->write.is_prep){
			ESP_LOGI(GATTS_TAG, "GATT_WRITE_EVT !isprep");
			struct gatts_chardesc_list *chardesc = getCharacteristicByHandle(param->write.handle, gl_profile_tab[PROFILE_A_APP_ID].chardesclist, gl_profile_tab[PROFILE_A_APP_ID].num_chars);
			ESP_LOGI(GATTS_TAG, "GATT_WRITE_EVT chardesc=%p", chardesc);
			if (chardesc) {
				ESP_LOGI(GATTS_TAG, "GATT_WRITE_EVT, value len %u, value :", param->write.len);
				esp_log_buffer_hex(GATTS_TAG, param->write.value, param->write.len);
				if (param->write.len < 1) break;
				if (param->write.len >= 11 && 0 == memcmp(param->write.value, "DeviceType;",11)) {
					ESP_LOGI(GATTS_TAG, "GATT_WRITE_EVT, DeviceType Query need_rsp: %d", param->write.need_rsp);
					
					esp_gatt_rsp_t gatt_rsp;
					esp_gatt_status_t status = ESP_GATT_OK;
					gatt_rsp.attr_value.handle = param->write.handle;
					gatt_rsp.attr_value.offset = 0;
					gatt_rsp.attr_value.auth_req = ESP_GATT_AUTH_REQ_NONE;
					gatt_rsp.attr_value.len = 18;
					memcpy(gatt_rsp.attr_value.value, "C:11:CCDBA75A337E;", gatt_rsp.attr_value.len);
					
					ESP_LOGI(GATTS_TAG, "GATT_WRITE_EVT, DeviceType Query Responded");
					esp_err_t response_err = esp_ble_gatts_send_response(gatts_if, param->write.conn_id, param->write.trans_id, status, &gatt_rsp);
					if (response_err != ESP_OK){
					   ESP_LOGE(GATTS_TAG, "Send response error %d\n", response_err);
					}
					ESP_LOGI(GATTS_TAG, "GATT_WRITE_EVT, DeviceType Query Responded 2");
					break;
				}
				parse_cmd(param->write.value, param->write.len);
				if (chardesc->handle == param->write.handle && param->write.len == 2){
					uint16_t descr_value = param->write.value[1]<<8 | param->write.value[0];
					if (descr_value == 0x0001){
						if (a1_property & ESP_GATT_CHAR_PROP_BIT_NOTIFY){
							ESP_LOGI(GATTS_TAG, "notify enable");
							uint8_t notify_data[15];
							for (int i = 0; i < sizeof(notify_data); ++i)
							{
								notify_data[i] = i%0xff;
							}
							//the size of notify_data[] need less than MTU size
							esp_ble_gatts_send_indicate(gatts_if, param->write.conn_id, chardesc->handle,
                                                sizeof(notify_data), notify_data, false);
						}
					} else if (descr_value == 0x0002) {
						if (a1_property & ESP_GATT_CHAR_PROP_BIT_INDICATE){
							ESP_LOGI(GATTS_TAG, "indicate enable");
							uint8_t indicate_data[15];
							for (int i = 0; i < sizeof(indicate_data); ++i) {
								indicate_data[i] = i%0xff;
							}
							//the size of indicate_data[] need less than MTU size
							esp_ble_gatts_send_indicate(gatts_if, param->write.conn_id, chardesc->handle,
                                                sizeof(indicate_data), indicate_data, true);
						}
					}
					else if (descr_value == 0x0000){
						ESP_LOGI(GATTS_TAG, "notify/indicate disable ");
					}else{
						ESP_LOGE(GATTS_TAG, "unknown descr value");
						esp_log_buffer_hex(GATTS_TAG, param->write.value, param->write.len);
					}
				}
            }
        }
        example_write_event_env(gatts_if, &a_prepare_write_env, param);
        break;
    }
    case ESP_GATTS_EXEC_WRITE_EVT:
        ESP_LOGI(GATTS_TAG,"ESP_GATTS_EXEC_WRITE_EVT");
        esp_ble_gatts_send_response(gatts_if, param->write.conn_id, param->write.trans_id, ESP_GATT_OK, NULL);
        example_exec_write_event_env(&a_prepare_write_env, param);
        break;
    case ESP_GATTS_MTU_EVT:
        ESP_LOGI(GATTS_TAG, "ESP_GATTS_MTU_EVT, MTU %d", param->mtu.mtu);
        break;
    case ESP_GATTS_UNREG_EVT:
        break;
    case ESP_GATTS_CREATE_EVT:
        ESP_LOGI(GATTS_TAG, "CREATE_SERVICE_EVT, status %d,  service_handle %d\n", param->create.status, param->create.service_handle);
        gl_profile_tab[PROFILE_A_APP_ID].service_handle = param->create.service_handle;
        esp_ble_gatts_start_service(gl_profile_tab[PROFILE_A_APP_ID].service_handle);

		regnum = 0;
		gatts_add_chars();
		break;
	case ESP_GATTS_ADD_INCL_SRVC_EVT:
		break;
	case ESP_GATTS_ADD_CHAR_EVT: {
		uint16_t length = 0;
		const uint8_t *prf_char;

        ESP_LOGI(GATTS_TAG, "ADD_CHAR_EVT, status %d,  attr_handle %d, service_handle %d, uuid16 %x\n",
                param->add_char.status, param->add_char.attr_handle, param->add_char.service_handle, param->add_char.char_uuid.uuid.uuid16);
		struct gatts_chardesc_list *chardesc = &gl_profile_tab[PROFILE_A_APP_ID].chardesclist[regnum];
		ESP_LOGI(GATTS_TAG, "ADD_CHAR_EVT, found characteric at %d", regnum);

		chardesc->handle = param->add_char.attr_handle;
		esp_err_t get_attr_ret = esp_ble_gatts_get_attr_value(param->add_char.attr_handle,  &length, &prf_char);
		if (get_attr_ret == ESP_FAIL){
			ESP_LOGE(GATTS_TAG, "ILLEGAL HANDLE");
		}

		ESP_LOGI(GATTS_TAG, "the gatts demo char length = %x\n", length);
		for(int i = 0; i < length; i++){
			ESP_LOGI(GATTS_TAG, "prf_char[%x] =%x\n",i,prf_char[i]);
		}
		regnum++;
		gatts_add_chars();
        break;
    }
    case ESP_GATTS_ADD_CHAR_DESCR_EVT:
        ESP_LOGI(GATTS_TAG, "ADD_DESCR_EVT, status %d, attr_handle %d, service_handle %d\n",
                 param->add_char_descr.status, param->add_char_descr.attr_handle, param->add_char_descr.service_handle);
		struct gatts_chardesc_list *chardesc = &gl_profile_tab[PROFILE_A_APP_ID].chardesclist[regnum];
		ESP_LOGI(GATTS_TAG, "ADD_DESCR_EVT, found characteric at %d", regnum);
		chardesc->handle = param->add_char_descr.attr_handle;
		regnum++;
		gatts_add_chars();
        break;
    case ESP_GATTS_DELETE_EVT:
        break;
    case ESP_GATTS_START_EVT:
        ESP_LOGI(GATTS_TAG, "SERVICE_START_EVT, status %d, service_handle %d\n",
                 param->start.status, param->start.service_handle);
        break;
    case ESP_GATTS_STOP_EVT:
        break;
    case ESP_GATTS_CONNECT_EVT: {
        esp_ble_conn_update_params_t conn_params = {0};
        memcpy(conn_params.bda, param->connect.remote_bda, sizeof(esp_bd_addr_t));
        /* For the IOS system, please reference the apple official documents about the ble connection parameters restrictions. */
        conn_params.latency = 0;
        conn_params.max_int = 0x20;    // max_int = 0x20*1.25ms = 40ms
        conn_params.min_int = 0x10;    // min_int = 0x10*1.25ms = 20ms
        conn_params.timeout = 400;    // timeout = 400*10ms = 4000ms
        ESP_LOGI(GATTS_TAG, "ESP_GATTS_CONNECT_EVT, conn_id %d, remote %02x:%02x:%02x:%02x:%02x:%02x:",
                 param->connect.conn_id,
                 param->connect.remote_bda[0], param->connect.remote_bda[1], param->connect.remote_bda[2],
                 param->connect.remote_bda[3], param->connect.remote_bda[4], param->connect.remote_bda[5]);
        gl_profile_tab[PROFILE_A_APP_ID].conn_id = param->connect.conn_id;
        //start sent the update connection parameters to the peer device.
        esp_ble_gap_update_conn_params(&conn_params);
        break;
    }
    case ESP_GATTS_DISCONNECT_EVT:
        ESP_LOGI(GATTS_TAG, "ESP_GATTS_DISCONNECT_EVT, disconnect reason 0x%x", param->disconnect.reason);
        esp_ble_gap_start_advertising(&adv_params);
        break;
    case ESP_GATTS_CONF_EVT:
        ESP_LOGI(GATTS_TAG, "ESP_GATTS_CONF_EVT, status %d attr_handle %d", param->conf.status, param->conf.handle);
        if (param->conf.status != ESP_GATT_OK){
            esp_log_buffer_hex(GATTS_TAG, param->conf.value, param->conf.len);
        }
        break;
    case ESP_GATTS_OPEN_EVT:
    case ESP_GATTS_CANCEL_OPEN_EVT:
    case ESP_GATTS_CLOSE_EVT:
		break;
    case ESP_GATTS_LISTEN_EVT:
		ESP_LOGI(GATTS_TAG, "ESP_GATTS_LISTEN_EVT, status %d attr_handle %d", param->conf.status, param->conf.handle);
		break;
    case ESP_GATTS_CONGEST_EVT:
    default:
        break;
    }
}

static void gatts_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param)
{
    /* If event is register event, store the gatts_if for each profile */
    if (event == ESP_GATTS_REG_EVT) {
        if (param->reg.status == ESP_GATT_OK) {
            gl_profile_tab[param->reg.app_id].gatts_if = gatts_if;
        } else {
            ESP_LOGI(GATTS_TAG, "Reg app failed, app_id %04x, status %d\n",
                    param->reg.app_id,
                    param->reg.status);
            return;
        }
    }

    /* If the gatts_if equal to profile A, call profile A cb handler,
     * so here call each profile's callback */
    do {
        int idx;
        for (idx = 0; idx < PROFILE_NUM; idx++) {
            if (gatts_if == ESP_GATT_IF_NONE || /* ESP_GATT_IF_NONE, not specify a certain gatt_if, need to call every profile cb function */
                    gatts_if == gl_profile_tab[idx].gatts_if) {
                if (gl_profile_tab[idx].gatts_cb) {
                    gl_profile_tab[idx].gatts_cb(event, gatts_if, param);
                }
            }
        }
    } while (0);
}


void timer_cb(void *arg) {
	int32_t time_since_boot = esp_timer_get_time() / 1000; // ms granularity is sufficient
	for (int i = 0 ; i < NUM_TCHANNELS ; i++) {
	    if (tchannels[i].timer == 0) continue;
	    if (tchannels[i].timer >= time_since_boot / 1000) continue;
	    tchannels[i].onoff = ! tchannels[i].onoff;
	    gpio_set_level(tchannels[i].gpio, tchannels[i].onoff);
	    tchannels[i].timer = 0;
	}
	
	//printf("TSB: %d\n", time_since_boot);
	for (int i = 0 ; i < NUM_CHANNELS ; i++) {
		int16_t high, low, delta;
		high = low = (int32_t)channels[i].limit * channels[i].high / 999;
		low = (int32_t)low * channels[i].low / 999;
		delta = high - low;
		if (channels[i].laststart == 0)
			channels[i].laststart = time_since_boot;
		int32_t pos = time_since_boot - channels[i].laststart;
		while ( pos > channels[i].periode ) {
			pos -= channels[i].periode;
			if (channels[i].count > 0) channels[i].count--;
			channels[i].laststart += channels[i].periode;
			//printf("New periode.\n");
		}
		uint32_t relpos = (int64_t)pos*0xffff/channels[i].periode;
		//if (i==0) printf("pos %ld relpos %ld\n", pos, relpos);
		uint16_t duty = 0;
		if (channels[i].count == 0) {
		    // count expired, keep at 0
		} else if (relpos < channels[i].attack) {
			duty = relpos * 0xffffu / channels[i].attack;
		} else if (relpos < channels[i].hold) {
			duty = 0xffffu;
		} else if (relpos < channels[i].decay) {
			duty = 0xffffu - (relpos - channels[i].hold) * 0xffffu / (channels[i].decay - channels[i].hold);
		} else {
			// hold 0 nothing to do
		}
		// if (i==0) printf("Duty: %d", duty);
		duty = (uint32_t)duty * (delta) / 0xffff + low;
		// if (i==0) printf(" %d\n", duty);
		ledc_set_duty(ledc_channel_template.speed_mode, channels[i].channel, duty);
		ledc_update_duty(ledc_channel_template.speed_mode, channels[i].channel);
	}
	fflush(stdout);
}

void app_main(void)
{
    esp_err_t ret;
    // Initialize NVS.
    ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK( ret );
    
    ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_CLASSIC_BT));
    esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
    ret = esp_bt_controller_init(&bt_cfg);
    if (ret) {
        ESP_LOGE(GATTS_TAG, "%s initialize controller failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }
    
    ret = esp_bt_controller_enable(ESP_BT_MODE_BLE);
    if (ret) {
        ESP_LOGE(GATTS_TAG, "%s enable controller failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }
    ret = esp_bluedroid_init();
    if (ret) {
        ESP_LOGE(GATTS_TAG, "%s init bluetooth failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }
    ret = esp_bluedroid_enable();
    if (ret) {
        ESP_LOGE(GATTS_TAG, "%s enable bluetooth failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }
    ret = esp_ble_gatts_register_callback(gatts_event_handler);
    if (ret){
        ESP_LOGE(GATTS_TAG, "gatts register error, error code = %x", ret);
        return;
    }
    ret = esp_ble_gap_register_callback(gap_event_handler);
    if (ret){
        ESP_LOGE(GATTS_TAG, "gap register error, error code = %x", ret);
        return;
    }

    ret = esp_ble_gatts_app_register(PROFILE_A_APP_ID);
    if (ret){
        ESP_LOGE(GATTS_TAG, "gatts app register error, error code = %x", ret);
        return;
    }
    esp_err_t local_mtu_ret = esp_ble_gatt_set_local_mtu(500);
    if (local_mtu_ret){
        ESP_LOGE(GATTS_TAG, "set local  MTU failed, error code = %x", local_mtu_ret);
    }

    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
#if 0
    printf("This is %s chip with %d CPU core(s), WiFi%s%s, ",
            CONFIG_IDF_TARGET,
            chip_info.cores,
            (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
            (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("silicon revision %d, ", chip_info.revision);
#endif

    uint32_t size_flash_chip;
    esp_flash_get_size(NULL, &size_flash_chip);
#if 0
    printf("%luMB %s flash\n", size_flash_chip / (1024 * 1024),
            (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

    printf("Minimum free heap size: %lu bytes\n", esp_get_minimum_free_heap_size());
#endif

    gpio_reset_pin(BLINK_GPIO);
    /* Set the GPIO as a push/pull output */
    gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);

	set_timer_freq(120);
	for (int i = 0 ; i < NUM_CHANNELS ; i++) {
		ledc_channel_template.channel  = channels[i].channel;
		ledc_channel_template.gpio_num = channels[i].gpio;
		ledc_channel_config(&ledc_channel_template);
	}
	for (int i = 0 ; i < NUM_TCHANNELS ; i++) {
	    gpio_set_direction(tchannels[i].gpio, GPIO_MODE_OUTPUT);
	    gpio_set_level(tchannels[i].gpio, 0);
	}
	esp_timer_init();
	ESP_LOGD(GATTS_TAG, "Timer init.\n");
	
	esp_timer_create_args_t targs = {
		.callback = timer_cb,
		.arg = NULL,
		.dispatch_method = ESP_TIMER_TASK,
		.name = "timer",
		.skip_unhandled_events = true
	};
	esp_timer_handle_t thandle;
	
	if (ESP_OK == esp_timer_create(&targs, &thandle)) {
		ESP_LOGD(GATTS_TAG, "Create timer ok.\n");
		esp_timer_start_periodic(thandle, 10000);
	} else {
		ESP_LOGE(GATTS_TAG, "Error creating timer.\n");
	}
	
	for (int i = 0 ; i < NUM_CHANNELS ; i++) {
		channels[i].mode = MP_TRIANGLE;
		channels[i].duty_cycle = 50;
		channels[i].periode = 1000+i*1000;
		setup_channel(&channels[i]);
	}
	ESP_LOGI(GATTS_TAG, "Startup complete.\n");
	uint8_t buffer[16];
	int bufpos = 0;
	while(true) {
		int x = getchar();
		if (x != -1) {
			switch(x) {
				case '\r':
				case '\n':
					if (bufpos) {
						printf("  Running: |%*.*s|\r\n",bufpos,bufpos,buffer);
						parse_cmd(buffer, bufpos);
					}
					bufpos = 0;
					break;
				default:
					if (bufpos >= sizeof(buffer)) bufpos = -1; // ignore until CR/LF
					if (bufpos >= 0) {
						buffer[bufpos++] = x;
						printf("%c",x);
					}
					break;
			}
			fflush(stdout);
		}
		vTaskDelay(1);
	}
    //esp_restart();
}

# esp32stroke

## Getting started

Go to the [build](build/Build.md) Directory to get instructions how to build the hardware.
For a first try we recommend the prebuilt [binaries](binaries/).

## Play with it

Go to https://elistan.gitlab.io/esp32stroke/strokegui.html or host the file anywhere you like.
The web page can connect to the device via serial (on PC) or BLE (on mobile).
If you want to be remotely control someone else's device or vice versa, click on your "link" on the bottom and share the resulting link copied to the clipboard. When someone opens that link, the two clients will sync via httprelay.io and the controls will be linked. Note that for safety reasons, limit and frequency are *not* synchronized.
Also note, that due to the way httprelay.io is implemented, you need to allow 3rd-party cookies for the page (click on the icon left of the URL in the browser and check the Cookies section). Otherwise expect strange behaviour especially of the chat.

## Play on SecondLife

Ask me inworld for the HUD and some decoration.
The HUD provides an URL for the control tools, which it will use to sync to inworld.
Feel free to add similar capabilities to other remote toy setups.

## Develop

git checkout https://gitlab.com/elistan/esp32stroke.git

## Support
contact me inworld or at elistan.octagon@gmail.com .

## Roadmap
- Compatibility to lovense on the bluetooth stack side (anyone got good docs on that?)
- build of the kivy for mobile (would the phone recognize the serial port?)

## Contributing

Sure. But please let's keep things as open as it can get. I suggest to fully put stuff in the public domain.
This is about having fun for everyone.

## Authors and acknowledgment
(insert your name here by contributing)

## License
Public Domain

## Project status
Actve, occasionally.
